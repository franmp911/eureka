FROM openjdk:8
ADD target/eureka.jar eureka.jar
EXPOSE 9999
ENTRYPOINT ["java", "-jar", "eureka.jar"]
